import { Mutations, MUTATIONS_TYPES } from "@/store/mutations";
import { ActionContext, ActionTree } from "vuex";
import { State } from "@/store/index";
import { api } from "@/api";

export const ACTIONS_TYPES = {
  FETCH_ITEMS: "fetchItems",
  SELECT_LINE: "selectLine",
  SELECT_STOP: "selectStop",
} as const;

type AugmentedActionContext = {
  commit<K extends keyof Mutations>(
    key: K,
    payload: Parameters<Mutations[K]>[1],
  ): ReturnType<Mutations[K]>;
} & Omit<ActionContext<State, State>, "commit">;

export interface Actions {
  [ACTIONS_TYPES.FETCH_ITEMS]({
    commit,
  }: AugmentedActionContext): Promise<void>;
  [ACTIONS_TYPES.SELECT_LINE](
    { commit }: AugmentedActionContext,
    { line }: { line: number | null },
  ): void;
  [ACTIONS_TYPES.SELECT_STOP](
    { commit }: AugmentedActionContext,
    { name }: { name: string | null },
  ): void;
}

export const actions: ActionTree<State, State> & Actions = {
  async [ACTIONS_TYPES.FETCH_ITEMS]({ commit }) {
    const response = await api.get("/stops");
    if (response.status === 200)
      commit(MUTATIONS_TYPES.SET_ITEMS, { items: response.data });
  },
  [ACTIONS_TYPES.SELECT_LINE]({ commit }, { line }) {
    commit(MUTATIONS_TYPES.SELECT_LINE, { line });
  },
  [ACTIONS_TYPES.SELECT_STOP]({ commit }, { name }) {
    commit(MUTATIONS_TYPES.SELECT_STOP, { name });
  },
};
