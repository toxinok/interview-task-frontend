import { MutationTree } from "vuex";
import { State } from "@/store/index";
import { ServerItems } from "@/types";

export const MUTATIONS_TYPES = {
  SET_ITEMS: "setItems",
  SELECT_LINE: "selectLine",
  SELECT_STOP: "selectStop",
} as const;

export type Mutations<S = State> = {
  [MUTATIONS_TYPES.SET_ITEMS](
    state: S,
    { items }: { items: ServerItems },
  ): void;
  [MUTATIONS_TYPES.SELECT_LINE](
    state: S,
    { line }: { line: number | null },
  ): void;
  [MUTATIONS_TYPES.SELECT_STOP](
    state: S,
    { name }: { name: string | null },
  ): void;
};

export const mutations: MutationTree<State> & Mutations = {
  [MUTATIONS_TYPES.SET_ITEMS](state, { items }) {
    state.items = items;
  },
  [MUTATIONS_TYPES.SELECT_LINE](state, { line }) {
    state.selectedLine = line;
  },
  [MUTATIONS_TYPES.SELECT_STOP](state, { name }) {
    state.selectedStop = name;
  },
};
