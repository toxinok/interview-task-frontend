import {
  CommitOptions,
  createStore,
  DispatchOptions,
  Store as VuexStore,
} from "vuex";
import { ServerItems } from "@/types";
import { Mutations, mutations } from "@/store/mutations";
import { Actions, actions } from "@/store/actions";
import { Getters, getters } from "@/store/getters";

export type State = {
  items: ServerItems | null;
  selectedLine: number | null;
  selectedStop: string | null;
};

export const store = createStore<State>({
  state: {
    items: null,
    selectedLine: null,
    selectedStop: null,
  },
  getters,
  mutations,
  actions,
  modules: {},
});

export type Store = Omit<
  VuexStore<State>,
  "getters" | "commit" | "dispatch"
> & {
  commit<K extends keyof Mutations, P extends Parameters<Mutations[K]>[1]>(
    key: K,
    payload: P,
    options?: CommitOptions,
  ): ReturnType<Mutations[K]>;
} & {
  dispatch<K extends keyof Actions>(
    key: K,
    payload?: Parameters<Actions[K]>[1],
    options?: DispatchOptions,
  ): ReturnType<Actions[K]>;
} & {
  getters: {
    [K in keyof Getters]: ReturnType<Getters[K]>;
  };
};

export function useStore() {
  return store as Store;
}
