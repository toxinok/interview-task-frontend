export type ServerItem = {
  line: number;
  stop: string;
  order: number;
  time: string;
};

export type ServerItems = ServerItem[];
