import { ServerItem } from "@/types";

export const createStopName = (item: ServerItem) => {
  const order = `${item.order < 10 ? "0" : ""}${item.order}`;
  return `${item.stop} ${order}`;
};

export const correctPolishLetters = (string: string) => {
  const dict: { [key: string]: string } = {
    ą: "a",
    ć: "c",
    ę: "e",
    ł: "l",
    ń: "n",
    ó: "o",
    ś: "s",
    ź: "z",
    ż: "z",
  };
  return string.replace(/[ąćęłńóśźż]/g, (match) => dict[match]);
};
