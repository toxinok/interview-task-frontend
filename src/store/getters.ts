import { State } from "@/store/index";
import { GetterTree } from "vuex";
import { ServerItems } from "@/types";
import { correctPolishLetters, createStopName } from "@/helpers";

export const GETTERS_TYPES = {
  ALL_LINES: "allLines",
  ALL_LINE_STOPS: "allLineStops",
  ALL_LINE_STOP_TIMES: "allLineStopTimes",
  ALL_STOPS: "allStops",
} as const;

export type Getters = {
  [GETTERS_TYPES.ALL_LINES](state: State): number[];
  [GETTERS_TYPES.ALL_LINE_STOPS](
    state: State,
  ): ({ line, order }: { line: number; order?: "asc" | "desc" }) => string[];
  [GETTERS_TYPES.ALL_LINE_STOP_TIMES](
    state: State,
  ): ({ line, stop }: { line: number; stop: string }) => string[];
  [GETTERS_TYPES.ALL_STOPS](
    state: State,
  ): ({
    order,
    search,
  }: {
    order?: "asc" | "desc";
    search?: string;
  }) => string[];
};

export const getters: GetterTree<State, State> & Getters = {
  [GETTERS_TYPES.ALL_LINES]: (state) => {
    const items = state.items!;
    const lines = items.map((item) => item.line);
    return [...new Set(lines)].sort();
  },
  [GETTERS_TYPES.ALL_LINE_STOPS]:
    (state) =>
    ({ line, order = "asc" }) => {
      const items = state.items!;
      const filteredStops = items.reduce<ServerItems>((accum, item) => {
        return item.line === line ? [...accum, item] : accum;
      }, []);
      const sortedStops = filteredStops.sort((a, b) =>
        a.order > b.order ? 1 : -1,
      );
      const values = [
        ...new Set(sortedStops.map((item) => createStopName(item))),
      ];
      return order === "asc" ? values : values.reverse();
    },
  [GETTERS_TYPES.ALL_LINE_STOP_TIMES]:
    (state) =>
    ({ line, stop }) => {
      const items = state.items!;

      const times = items.reduce<string[]>((accum, item) => {
        return item.line === line && createStopName(item) === stop
          ? [...accum, item.time]
          : accum;
      }, []);
      return [...times].sort();
    },
  [GETTERS_TYPES.ALL_STOPS]:
    (state) =>
    ({ order = "asc", search = "" }) => {
      const items = state.items!;

      const filteredItems = items.filter((item) => {
        const stop = item.stop;

        const initial = correctPolishLetters(stop.toLowerCase());
        const searched = correctPolishLetters(search.trim().toLowerCase());

        return search === "" || initial.includes(searched);
      });

      const stops = filteredItems.map((item) => item.stop);

      const values = [...new Set(stops)].sort();
      return order === "asc" ? values : values.reverse();
    },
};
