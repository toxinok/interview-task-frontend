import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import BusLines from "@/components/BusLines/BusLines.vue";
import BusStops from "@/components/Stops/BusStops.vue";

export const ROUTE_PATHS = {
  home: "/",
  stops: "/stops",
} as const;

export const routes: Array<RouteRecordRaw> = [
  { path: `${ROUTE_PATHS.home}`, component: BusLines, name: "Bus Lines" },
  { path: ROUTE_PATHS.stops, component: BusStops, name: "Stops" },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
